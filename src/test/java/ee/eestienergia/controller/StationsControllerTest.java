package ee.eestienergia.controller;

import ee.eestienergia.handler.StationResponseExceptionHandler;
import ee.eestienergia.init.StationsApiInitializer;
import ee.eestienergia.service.StationsService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = StationsApiInitializer.AppConfiguration.class)
public class StationsControllerTest {

    private static final String STATION_NAME = "Narva";

    @Autowired
    private StationsController stationsController;

    @Autowired
    private StationResponseExceptionHandler stationResponseExceptionHandler;

    @MockBean
    private StationsService stationsService;

    private MockMvc mockMvc;

    @Before
    public void setUp() {
        mockMvc = standaloneSetup(stationsController)
                .setControllerAdvice(stationResponseExceptionHandler)
                .build();
        stationsController = mock(StationsController.class);
    }

    @Test
    public void testStationNames() throws Exception {
        mockMvc.perform(get("/stations"))
                .andExpect(status().isOk());

        verify(stationsService, times(1)).getStationNames();
    }

    @Test
    public void testStationDataByName() throws Exception {
        mockMvc.perform(get("/{stationName}", STATION_NAME))
                .andExpect(status().isOk());

        verify(stationsService, times(1)).getStationDataByName(eq(STATION_NAME));
    }

}
