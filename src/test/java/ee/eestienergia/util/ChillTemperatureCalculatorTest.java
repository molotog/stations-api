package ee.eestienergia.util;

import ee.eestienergia.model.Station;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class ChillTemperatureCalculatorTest {

    private ChillTemperatureCalculator chillTemperatureCalculator;

    @Before
    public void setup() {
        chillTemperatureCalculator = new ChillTemperatureCalculator();
    }

    @Test
    public void testChillTemperature() {
        Station station = createStation();
        station = chillTemperatureCalculator.calculateTemperature(station);
        assertEquals(new Double("13.36"), station.getChillTemperature());
    }

    @Test
    public void testChillTemperatureNotCalculated() {
        Station station = new Station();
        station = chillTemperatureCalculator.calculateTemperature(station);
        assertNull(station.getChillTemperature());
    }

    private Station createStation() {
        Station station = new Station();
        station.setAirTemperature(new Double("15.0"));
        station.setWindSpeed(new Double("7.0"));
        return station;
    }
}
