package ee.eestienergia.exception;

public class StationServiceNotAvailableException extends RuntimeException {

    private final String systemMessage;

    public StationServiceNotAvailableException(final String systemMessage) {
        super();
        this.systemMessage = systemMessage;
    }

    public String getSystemMessage() {
        return systemMessage;
    }
}
