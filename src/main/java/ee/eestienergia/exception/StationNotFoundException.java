package ee.eestienergia.exception;

public class StationNotFoundException extends RuntimeException {

    private final String systemMessage;

    public StationNotFoundException(final String systemMessage) {
        super();
        this.systemMessage = systemMessage;
    }

    public String getSystemMessage() {
        return systemMessage;
    }
}
