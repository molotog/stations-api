package ee.eestienergia.util;

import ee.eestienergia.model.Station;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Component("chillTemperatureCalculator")
public class ChillTemperatureCalculator {

    private static final int MINUTES_IN_HOUR = 3600;
    private static final int METERS_IN_KM = 1000;

    private static final int ROUND_SCALE = 2;

    private static final double FN_EXPONENT = 0.16;
    private static final double FN_PARAMETER_1 = 13.12;
    private static final double FN_PARAMETER_2 = 0.6215;
    private static final double FN_PARAMETER_3 = 11.37;
    private static final double FN_PARAMETER_4 = 0.3965;

    public Station calculateTemperature(final Station station) {
        Double airTemperature = station.getAirTemperature();
        Double windSpeed = station.getWindSpeed();

        if (airTemperature == null || windSpeed == null) {
            return station;
        }

        Double kmphWindSpeed = windSpeed * MINUTES_IN_HOUR / METERS_IN_KM;

        Double v = Math.pow(kmphWindSpeed, FN_EXPONENT);

        Double tChill = FN_PARAMETER_1
                + FN_PARAMETER_2 * airTemperature
                - FN_PARAMETER_3 * v
                + FN_PARAMETER_4 * airTemperature * v;

        station.setChillTemperature(round(tChill));
        return station;
    }

    private double round(final Double value) {
        BigDecimal result = new BigDecimal(String.valueOf(value));
        result = result.setScale(ROUND_SCALE, RoundingMode.CEILING);
        return result.doubleValue();
    }

}
