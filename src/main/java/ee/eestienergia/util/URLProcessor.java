package ee.eestienergia.util;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

@Component("urlProcessor")
public class URLProcessor {

    private static final String USER_AGENT =
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36";

    public HttpURLConnection createHttpURLConnection(final String urlString) throws Exception {
        try {
            HttpURLConnection conn = (HttpURLConnection) createUrl(urlString).openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("User-Agent", USER_AGENT);
            if (conn.getResponseCode() != HttpStatus.OK.value()) {
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }
            return conn;
        } catch (IOException e) {
            throw new Exception("Failed to establish the connection.", e);
        }
    }

    private URL createUrl(final String urlString) throws Exception {
        try {
            return new URL(urlString);
        } catch (MalformedURLException e) {
            throw new Exception("Observations service is currently unavailable, please try again later.", e);
        }
    }
}
