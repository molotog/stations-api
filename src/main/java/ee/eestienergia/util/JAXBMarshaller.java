package ee.eestienergia.util;

import org.springframework.stereotype.Component;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.InputStream;

@Component("jaxbMarshaller")
public class JAXBMarshaller {

    public <T> Object getObjectFromXml(final InputStream is, final Class<T> clazz) throws Exception {
        try {
            JAXBContext jc = JAXBContext.newInstance(clazz);
            Unmarshaller unmarshaller = jc.createUnmarshaller();
            return unmarshaller.unmarshal(is);
        } catch (JAXBException e) {
            throw new Exception("Failed to unmarshall observations xml data.", e);
        }
    }

}
