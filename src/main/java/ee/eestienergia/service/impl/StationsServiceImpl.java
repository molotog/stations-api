package ee.eestienergia.service.impl;

import ee.eestienergia.exception.StationNotFoundException;
import ee.eestienergia.exception.StationServiceNotAvailableException;
import ee.eestienergia.model.Observations;
import ee.eestienergia.model.Station;
import ee.eestienergia.service.StationsService;
import ee.eestienergia.util.ChillTemperatureCalculator;
import ee.eestienergia.util.JAXBMarshaller;
import ee.eestienergia.util.URLProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class StationsServiceImpl implements StationsService {

    private final ChillTemperatureCalculator chillTemperatureCalculator;
    private final URLProcessor urlProcessor;
    private final JAXBMarshaller jaxbMarshaller;
    private final String observationUrl;

    @Autowired
    public StationsServiceImpl(final ChillTemperatureCalculator chillTemperatureCalculator,
                               final URLProcessor urlProcessor,
                               final JAXBMarshaller jaxbMarshaller,
                               @Value("${observations.url}") final String observationUrl) {
        this.chillTemperatureCalculator = chillTemperatureCalculator;
        this.urlProcessor = urlProcessor;
        this.jaxbMarshaller = jaxbMarshaller;
        this.observationUrl = observationUrl;
    }

    @Override
    public Station getStationDataByName(final String stationName) throws StationNotFoundException {
        Observations observations = getObservations();
        if (observations == null) {
            throw new StationNotFoundException("Service unavailable");
        }

        Optional<Station> station = findStation(observations, stationName);

        return station.orElseThrow(() -> new StationNotFoundException("No station with this name found"));

    }

    @Override
    public List<String> getStationNames() {
        Observations observations = getObservations();
        return observations == null ? Collections.emptyList()
                : observations.getStations().stream().map(Station::getName).collect(Collectors.toList());
    }

    private Optional<Station> findStation(final Observations observations, final String stationName) {
        return observations.getStations().stream()
                .filter(station -> stationName.equals(station.getName()))
                .map(chillTemperatureCalculator::calculateTemperature)
                .findFirst();
    }

    private Observations getObservations() throws StationServiceNotAvailableException {
        HttpURLConnection conn;
        try {
            conn = urlProcessor.createHttpURLConnection(observationUrl);
        } catch (Exception e) {
            throw new StationServiceNotAvailableException("Cannot acquire connection to " + observationUrl + ".");
        }

        try (InputStream is = conn.getInputStream()) {
            return (Observations) jaxbMarshaller.getObjectFromXml(is, Observations.class);
        } catch (Exception e) {
            throw new StationServiceNotAvailableException("Error occured then processing data.");
        } finally {
            conn.disconnect();
        }
    }

}
