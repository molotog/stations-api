package ee.eestienergia.service;

import ee.eestienergia.exception.StationNotFoundException;
import ee.eestienergia.model.Station;

import java.util.List;

public interface StationsService {

    /**
     * Get station by name.
     *
     * @param stationName station name
     * @return station
     * @throws StationNotFoundException exception
     */
    Station getStationDataByName(String stationName) throws StationNotFoundException;

    /**
     * Get all station names.
     *
     * @return station names
     */
    List<String> getStationNames();
}
