package ee.eestienergia.error;

import com.fasterxml.jackson.annotation.JsonProperty;

public class StationErrorResponse {

    @JsonProperty("userMessage")
    private String userMessage;

    @JsonProperty("systemMessage")
    private String systemMessage;

    public String getUserMessage() {
        return userMessage;
    }

    public void setUserMessage(String userMessage) {
        this.userMessage = userMessage;
    }

    public String getSystemMessage() {
        return systemMessage;
    }

    public void setSystemMessage(String systemMessage) {
        this.systemMessage = systemMessage;
    }


    public static final class Builder {
        private String userMessage;
        private String systemMessage;

        private Builder() {
        }

        public static StationErrorResponse.Builder Builder() {
            return new StationErrorResponse.Builder();
        }

        public StationErrorResponse.Builder withUserMessage(String userMessage) {
            this.userMessage = userMessage;
            return this;
        }

        public StationErrorResponse.Builder withSystemMessage(String systemMessage) {
            this.systemMessage = systemMessage;
            return this;
        }

        public StationErrorResponse build() {
            StationErrorResponse apiMessage = new StationErrorResponse();
            apiMessage.setUserMessage(this.userMessage);
            apiMessage.setSystemMessage(this.systemMessage);
            return apiMessage;
        }
    }

}
