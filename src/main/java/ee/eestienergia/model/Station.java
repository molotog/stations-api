package ee.eestienergia.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "station")
public class Station {

    private String name;
    private Integer wmocode;
    private Double longitude;
    private Double latitude;
    private String phenomenon;
    private Double visibility;
    private Double precipitations;
    private Double airpressure;
    private Integer relativehumidity;
    private Double airTemperature;
    private Integer winddirection;
    private Double windSpeed;
    private Double windspeedmax;
    private Double waterlevel;
    private Double watertemperature;
    private Double chillTemperature;

    @XmlElement(name = "name")
    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    @XmlElement(name = "wmocode")
    public Integer getWmocode() {
        return wmocode;
    }

    public void setWmocode(final Integer wmocode) {
        this.wmocode = wmocode;
    }

    @XmlElement(name = "longitude")
    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(final Double longitude) {
        this.longitude = longitude;
    }

    @XmlElement(name = "latitude")
    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(final Double latitude) {
        this.latitude = latitude;
    }

    @XmlElement(name = "phenomenon")
    public String getPhenomenon() {
        return phenomenon;
    }

    public void setPhenomenon(final String phenomenon) {
        this.phenomenon = phenomenon;
    }

    @XmlElement(name = "visibility")
    public Double getVisibility() {
        return visibility;
    }

    public void setVisibility(final Double visibility) {
        this.visibility = visibility;
    }

    @XmlElement(name = "precipitations")
    public Double getPrecipitations() {
        return precipitations;
    }

    public void setPrecipitations(final Double precipitations) {
        this.precipitations = precipitations;
    }

    @XmlElement(name = "airpressure")
    public Double getAirpressure() {
        return airpressure;
    }

    public void setAirpressure(final Double airpressure) {
        this.airpressure = airpressure;
    }

    @XmlElement(name = "relativehumidity")
    public Integer getRelativehumidity() {
        return relativehumidity;
    }

    public void setRelativehumidity(final Integer relativehumidity) {
        this.relativehumidity = relativehumidity;
    }

    @XmlElement(name = "airtemperature")
    public Double getAirTemperature() {
        return airTemperature;
    }

    public void setAirTemperature(final Double airTemperature) {
        this.airTemperature = airTemperature;
    }

    @XmlElement(name = "winddirection")
    public Integer getWinddirection() {
        return winddirection;
    }

    public void setWinddirection(final Integer winddirection) {
        this.winddirection = winddirection;
    }

    @XmlElement(name = "windspeed")
    public Double getWindSpeed() {
        return windSpeed;
    }

    public void setWindSpeed(final Double windSpeed) {
        this.windSpeed = windSpeed;
    }

    @XmlElement(name = "windspeedmax")
    public Double getWindspeedmax() {
        return windspeedmax;
    }

    public void setWindspeedmax(final Double windspeedmax) {
        this.windspeedmax = windspeedmax;
    }

    @XmlElement(name = "waterlevel")
    public Double getWaterlevel() {
        return waterlevel;
    }

    public void setWaterlevel(final Double waterlevel) {
        this.waterlevel = waterlevel;
    }

    @XmlElement(name = "watertemperature")
    public Double getWatertemperature() {
        return watertemperature;
    }

    public void setWatertemperature(final Double watertemperature) {
        this.watertemperature = watertemperature;
    }

    public Double getChillTemperature() {
        return chillTemperature;
    }

    public void setChillTemperature(final Double chillTemperature) {
        this.chillTemperature = chillTemperature;
    }
}
