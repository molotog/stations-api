package ee.eestienergia.model;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "observations")
public class Observations {

    private long timestamp;

    private List<Station> stations;

    @XmlAttribute(name = "timestamp")
    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(final long timestamp) {
        this.timestamp = timestamp;
    }

    @XmlElement(name = "station")
    public List<Station> getStations() {
        return stations;
    }

    public void setStations(final List<Station> stations) {
        this.stations = stations;
    }
}
