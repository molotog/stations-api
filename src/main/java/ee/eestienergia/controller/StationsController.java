package ee.eestienergia.controller;

import ee.eestienergia.model.Station;
import ee.eestienergia.service.StationsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class StationsController {

    private StationsService stationsService;

    @Autowired
    StationsController(final StationsService stationsService) {
        this.stationsService = stationsService;
    }

    /**
     * Get all station names.
     *
     * @return stations names
     */
    @CrossOrigin
    @RequestMapping(value = "/stations", method = RequestMethod.GET, produces = "application/json")
    public List<String> findStationNames() {
        return stationsService.getStationNames();
    }

    /**
     * Get stations by name.
     *
     * @param stationName station name
     * @return station
     */
    @CrossOrigin
    @RequestMapping(value = "/{stationName}", method = RequestMethod.GET, produces = "application/json")
    public Station findStationByName(@PathVariable("stationName") final String stationName) {
        return stationsService.getStationDataByName(stationName);
    }

}
