package ee.eestienergia.init;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

public class StationsApiInitializer {

    @SpringBootApplication(scanBasePackages = {"ee.eestienergia.*"})
    public static class AppConfiguration {
    }

    public static void main(String[] args) throws Exception {
        SpringApplication.run(AppConfiguration.class, args);
    }

}
