package ee.eestienergia.handler;

import ee.eestienergia.error.StationErrorResponse;
import ee.eestienergia.exception.StationNotFoundException;
import ee.eestienergia.exception.StationServiceNotAvailableException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class StationResponseExceptionHandler {

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = { StationNotFoundException.class })
    @ResponseBody
    protected StationErrorResponse handleStationNotFound(StationNotFoundException e) {
        return StationErrorResponse.Builder.Builder()
                .withUserMessage("Station with such name was not found")
                .withSystemMessage(e.getSystemMessage())
                .build();
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(value = { StationServiceNotAvailableException.class })
    @ResponseBody
    protected StationErrorResponse handleStationServiceNotAvailable(StationServiceNotAvailableException e) {
        return StationErrorResponse.Builder.Builder()
                .withUserMessage("Sorry, stations service is currently unavailable, please try again later.")
                .withSystemMessage(e.getSystemMessage())
                .build();
    }

}
